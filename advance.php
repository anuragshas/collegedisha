<?php
session_start();
if (!empty($_POST['rank']))
    $rank = $_POST['rank'];
else
    $rank = $_SESSION['r'];
if (!isset($_SESSION['auth'])) {
    if (!empty($_POST['email']))
        $email = $_POST['email'];
    else
        $email = "";
    if (!isset($_SESSION['e']))
        $_SESSION['e'] = $email;
    else
        $email = $_SESSION['e'];
}
if (!empty($_POST['cat']))
    $cat = $_POST['cat'];
else
    $cat = $_SESSION['c'];

if (!isset($_SESSION['c']))
    $_SESSION['c'] = $cat;
if (!isset($_SESSION['r']))
    $_SESSION['r'] = $rank;
switch ($cat) {
    case 1:
        $tname = "ja_general";
        $cname = "OpenRank";
        break;
    case 2:
        $tname = "ja_general";
        $cname = "PWDRank";
        break;
    case 3:
        $tname = "ja_obc";
        $cname = "OpenRank";
        break;
    case 4:
        $tname = "ja_obc";
        $cname = "PWDRank";
        break;
    case 5:
        $tname = "ja_sc";
        $cname = "OpenRank";
        break;
    case 6:
        $tname = "ja_sc";
        $cname = "PWDRank";
        break;
    case 7:
        $tname = "ja_st";
        $cname = "OpenRank";
        break;
    case 8:
        $tname = "ja_st";
        $cname = "PWDRank";
        break;
}
require_once "DBConnection.php";
$dbconnection = new DBConnection();
$connection = $dbconnection->connect();
$link = $connection[0];
$db = $connection[1];
//Create query
$qry = 'SELECT institute_name,branch_name,' . $cname . ' FROM institute i inner join ' . $tname . ' b on i.id=b.id where ' . $cname . '>=' . $rank . ' and ' . $cname . ' > 0 order by ' . $cname;

$_SESSION['qry'] = $qry;
//Execute query
$result = mysqli_query($link, $qry);
require("navbar.php");
?>


<div class="row">
    <h2 class="center-align" id="topmsg">JEE-ADVANCED COLLEGE PREDICTOR</h2>
    <div class="amber darken-2 headline "></div>
</div>
<br>

<div class="row">
    <div class="col s12 m12 l3">
        <div class="row">
            <div class="col s12 blue-grey darken-4 white-text">
                <h5 class="center-align">Try our Modified Search</h5>
            </div>
        </div>
        <div class="row">
            <form class="col s12" action="advance_filter.php" method="post">
                <div class="row">
                    <p>
                        <input name="group1" class="g1" type="radio" id="test1" value="insname" required/>
                        <label for="test1" id="insti" class="input-field col s12">

                            <select multiple name="iname[]" id="ins">
                                <option value="" disabled selected>I want this Institute</option>
                                <?php
                                $qry = 'SELECT distinct(institute_name) as institute_name FROM institute where id > 386 order by institute_name'; //Execute query
                                $result1 = mysqli_query($link, $qry);
                                while ($row = mysqli_fetch_assoc($result1)) {
                                    echo '<option value="' . $row['institute_name'] . '">' . $row['institute_name'] . '</option>';
                                }

                                ?>
                            </select>
                            <!-- <label>Institute Name</label> -->

                        </label>
                    </p>
                </div>
                <div class="row">
                    <p>
                        <input name="group1" class="g1" type="radio" id="test2" value="brname" required/>
                        <label for="test2" id="branch" class="input-field col s12">
                            <select multiple name="bname[]" id="bran">
                                <option value="" disabled selected>I want this Branch</option>
                                <?php
                                $qry = 'CALL getAdvancedBranch()'; //Execute query
                                $result1 = mysqli_query($link, $qry);
                                while ($row = mysqli_fetch_assoc($result1)) {
                                    echo '<option value="' . $row['branch_name'] . '">' . $row['branch_name'] . '</option>';
                                }

                                ?>
                            </select>
                            <!-- <label>Branch Name</label> -->

                        </label>
                    </p>
                </div>
                <br>
                <div class="row">
                    <div class="col s12">
                        <div class="center-align">
                            <button class="btn waves-effect waves-light " type="submit" name="filter"><i
                                        class="material-icons right">filter_list</i>Filter
                            </button>
                        </div>
                    </div>
                </div>
            </form>
            <div class="row">
                <div class="col s12">
                    <div class="center-align"><a id="res" class="btn waves-effect waves-light red lighten-2"><i
                                    class="material-icons right">restore</i>Reset</a></div>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <div class="center-align"><a href="index.php#predictor" class="btn waves-effect waves-light">Other
                            Predictors</a></div>
                </div>
            </div>
        </div>
    </div>


    <div class="col s12 m12 l8">

        <?php
        echo '<table class="bordered striped" id="advtable">
<thead><tr><th>Institute Name</th>
<th>Branch Name</th>
<th>Closing Ranks</th></tr></thead><tbody>';
        //Show the rows in the fetched resultset one by one
        while ($row = mysqli_fetch_assoc($result)) {
            echo '<tr>
<td>' . $row['institute_name'] . '</td>
<td>' . $row['branch_name'] . '</td>
<td>' . $row[$cname] . '</td>
</tr>';
        }
        echo '</tbody></table>';
        mysqli_close($link);
        ?>
    </div>
</div>

<?php require("footer.php"); ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/js/materialize.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.11/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script src="./js/additional-methods.min.js"></script>
<script>
    $(document).ready(function () {
        $(".button-collapse").sideNav();
        $('select').material_select();
        $('.modal-trigger').leanModal();
        $('.parallax').parallax();
        $('#advtable').dataTable({
            responsive: true,
            "bLengthChange": false,
            "iDisplayLength": 8,
            "aaSorting": [],
            "sDom": '<"row dt"<"col s12 m4 l4" f>p>r<"dt" t i>l<"clear">'
        });
        $('#advtable_filter label').addClass('slabel');
    });
    $('#res').click(function () {
        location.reload();
    });
</script>
<script src="./js/login-regis.js" async></script>
<script type="text/javascript">
    $("#test1").click(function () {
        $("#test2").prop("disabled", true);
        var op = document.getElementById("branch").getElementsByTagName("input");
        for (var i = 0; i < op.length; i++) {
            op[i].disabled = true;
        }
    });
    document.getElementById("test2").addEventListener("click", function () {
        document.getElementById("test1").setAttribute("disabled", "true");
        var op = document.getElementById("insti").getElementsByTagName("input");
        for (var i = 0; i < op.length; i++) {
            op[i].disabled = true;
        }
    });
</script>

</body>
</html>