<?php
	if(!($url_name=='/forum.php' || $url_name=='/user_panel.php')){
		echo'<div class="parallax-container">
		<h4 class="center-align"><strong> Ask, Answer &amp; Discuss</strong></h4>
		<br>
		<h5 class="center-align">Get the Best Opinion and Advice from Experts!!</h5>';
		if(!isset($_SESSION['auth']))
			echo'<br><div class="center-align"><a class="waves-effect waves-light btn modal-trigger" data-target="log">Login</a></div>';
		else if($_SESSION['auth']==1)
			echo'<br><div class="center-align"><a class="waves-effect waves-light btn" href="forum.php">Ask</a></div>';
		else
			echo'<br><div class="center-align"><a class="waves-effect waves-light btn" href="forum.php">Forum</a></div>';
		echo'<div class="parallax"><img src="./images/112.webp" alt=""></div>
		</div>
			<div class="news ">
				<h4><strong> Latest News and Updates</strong></h4>
				<div class="container">
					<div class="row">
						<div class="col s12 m6 l4">
							<div class="card newscard hoverable">
								<div class="card-content white-text">
									<span class="card-title"><small>CBSE Class XII Board Results announced!</small></span>
								</div>
								<div class="card-action">
									<a class="modal-trigger" data-target="modal4">Read More</a>                   
								</div>
							</div>
						</div>
						<div class="col s12 m6 l4">
							<div class="card newscard hoverable">
								<div class="card-content white-text">
									<span class="card-title"><small>BITSAT Application Date extended for one more week</small></span>
								</div>
								<div class="card-action">
									<a class="modal-trigger" data-target="modal6">Read More</a>
								</div>
							</div>
						</div>
						<div class="col s12 m12 l4">
							<div class="card newscard hoverable">
								<div class="card-content white-text">
									<span class="card-title"><small>Jee-Advanced 2016 Results are Out!!</small></span>
								</div>
								<div class="card-action">
									<a class="modal-trigger" data-target="modal5">Read More</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div id="modal4" class="modal">
				<div class="modal-content">
					<h4>CBSE Class XII Board Results announced!</h4>
					<p>CBSE announced its Class XII Board Results for all streams. You can check Your result at the Official Website of CBSE </p>
				</div>
			</div>
			<div id="modal5" class="modal">
				<div class="modal-content">
					<h4>Jee-Advance 2016 Results are out!</h4>
					<p>Jee Advanced 2016 results were announced on Sunday 12th June, Aman Bansal from Jaipur tops the examination.<br/>Check Your chances of getting into IITs using our aaccurate predictors! </p>
				</div>
			</div>
			<div id="modal6" class="modal">
				<div class="modal-content">
					<h4>BITSAT Application Date extended for one more week</h4>
					<p>Application dates for applying to BITSAT has been extended for one more week.For more detais visit BITSAT Website!</p>
				</div>
			</div>';
	}
?>

	<div class="fixed-action-btn">
		<a class="btn-floating btn-large"><i class="large material-icons">chat</i>
		</a>
		<ul>
			<li><a class="btn-floating" href="https://plus.google.com/u/0/107803766478253459719/posts" target="_blank"><i class="fa fa-google-plus-square fa-3x"></i></a></li>
			<li><a class="btn-floating" href="https://www.facebook.com/collegedisha.in/" target="_blank"><i class="fa fa-facebook-square fa-3x"></i></a></li>
			<li><a class="btn-floating"><i class="fa fa-linkedin fa-3x"></i></a></li>
		</ul>
	</div>

	<footer class="page-footer" id="ftt">
		<div class="container">
			<div class="row">
				<div class="col l4 s12">
						<h5 class="white-text">Connect With Us</h5>
						<p>
						<a href="#"><i class="fa fa-linkedin-square fa-2x"></i></a>&nbsp;&nbsp;&nbsp;
						<a href="https://www.facebook.com/collegedisha.in/" target="_blank"><i class="fa fa-facebook-square fa-2x"></i></a>&nbsp;&nbsp;&nbsp;
						<a href="https://plus.google.com/u/0/107803766478253459719/posts" target="_blank"><i class="fa fa-google-plus-square fa-2x"></i></a>
						</p>
				</div>
				<div class="col l3 push-l5 s12">
					<h5 class="white-text">About Us</h5>
					<p class="white-text">College Disha- started by a team of young Engineers, with a goal of helping the engineering aspirants.</p>
				</div>
			</div>
			<div class="red-text">Disclaimer: Prediction of colleges is based on JEE Main 2014 Closing Rank Reports by CSAB, Jee Advanced 2014 closing ranks  by Josaa and BITSAT 2015 closing marks <br>Please note that the displayed colleges and branches are only for reference purpose.Collegedisha.in does not guarantee any seat in any Institute </div>
		</div>

		<div class="footer-copyright">
			<div class="row">
				<div class="col s12">
					<a href="contact-us.php">Contact Us</a><span class="linkdivider"></span><a href="privacy-policy.php">Privacy Policy</a>
				</div>
			</div>
		</div>
	</footer>