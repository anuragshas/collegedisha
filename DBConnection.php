<?php


class DBConnection {
    public function __construct()
    {

    }

    public function connect()
    {
        $link=mysqli_connect( 'localhost', 'root', ''); //Check link to the mysql server
        if(!$link) die('Failed to connect to server:' . mysqli_error());
        $db=mysqli_select_db($link, 'college');
        if(!$db) die( "Unable to select database");
        return [$link,$db];
    }
}