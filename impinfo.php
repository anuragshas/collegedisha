<?php
session_start();
require_once "DBConnection.php";
$dbconnection = new DBConnection();
$connection = $dbconnection->connect();
$link = $connection[0];
$db = $connection[1];
$qryadv = 'SELECT DISTINCT(institute_details.institute_name),institute_details.state,institute_details.address,institute_details.website FROM institute_details INNER JOIN institute ON institute.institute_name=institute_details.institute_name where ID>=387 AND ID<=603';
$result1 = mysqli_query($link, $qryadv);

$qrymains = 'SELECT DISTINCT(institute_details.institute_name),institute_details.state,institute_details.address,institute_details.website FROM institute_details INNER JOIN institute ON institute.institute_name=institute_details.institute_name where ID>=1 AND ID<=349';
$result2 = mysqli_query($link, $qrymains);

$qrybits = 'SELECT DISTINCT(institute_details.institute_name),institute_details.state,institute_details.address,institute_details.website FROM institute_details INNER JOIN institute ON institute.institute_name=institute_details.institute_name where ID>=350 AND ID<=386';
$result3 = mysqli_query($link, $qrybits);
require("navbar.php");
?>
<div class="row">
    <h2 class="center" id="topmsg">COLLEGE DETAILS</h2>
    <div class="amber darken-2 headline "></div>
</div>
<br>
<div class="container">
    <div class="row">
        <div class="col s12">
            <ul class="tabs">
                <li class="tab col s3"><a class="active" href="#mainstab">JEE-MAINS</a></li>
                <li class="tab col s3"><a href="#advtab">JEE-ADVANCED</a></li>
                <li class="tab col s3"><a href="#bitstab">BITS</a></li>
            </ul>
        </div>
        <div id="mainstab" class="col s12">
            <br>
            <?php
            echo '<table class="bordered striped" id="mainstable">
						<thead><tr><th>Institute Name</th>
						<th>State</th>
						<th>Address</th>
						</tr></thead><tbody>';
            while ($row = mysqli_fetch_assoc($result2)) {
                echo '<tr>
							<td>' . $row['institute_name'] . '</td>
							<td>' . $row['state'] . '</td>
							<td>' . $row['address'] . '</td>
							</tr>';
            }
            echo '</tbody></table>';
            ?>
        </div>
        <div id="advtab" class="col s12">
            <br>
            <?php
            echo '<table class="bordered striped" id="advtable">
							<thead><tr><th>Institute Name</th>
							<th>State</th>
							<th>Address</th>
							</tr></thead><tbody>';
            while ($row = mysqli_fetch_assoc($result1)) {
                echo '<tr>
							<td>' . $row['institute_name'] . '</td>
							<td>' . $row['state'] . '</td>
							<td>' . $row['address'] . '</td>
							</tr>';
            }
            echo '</tbody></table>';
            ?>
        </div>
        <div id="bitstab" class="col s12">
            <br>
            <?php
            echo '<table class="bordered striped" id="bitstable">
						<thead><tr><th>Institute Name</th>
						<th>State</th>
						<th>Address</th>
						</tr></thead><tbody>';
            while ($row = mysqli_fetch_assoc($result3)) {
                echo '<tr>
							<td>' . $row['institute_name'] . '</td>
							<td>' . $row['state'] . '</td>
							<td>' . $row['address'] . '</td>
							</tr>';
            }
            echo '</tbody></table>';
            mysqli_close($link);
            ?>
        </div>
    </div>
</div>
<br>
<?php require("footer.php"); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/js/materialize.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.11/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script src="./js/additional-methods.min.js"></script>
<script>
    $(document).ready(function () {
        $(".button-collapse").sideNav();
        $('.modal-trigger').leanModal();
        $('.parallax').parallax();
        $('#advtable').dataTable({
            responsive: true,
            "bLengthChange": false,
            "iDisplayLength": 20,
            "sDom": '<"row dt"<"col s12 m4 l4" f>p>r<"dt" t i>l<"clear">'
        });
        $('#advtable_filter label').addClass('slabel');
        $('#mainstable').dataTable({
            responsive: true,
            "bLengthChange": false,
            "iDisplayLength": 20,
            "sDom": '<"row dt"<"col s12 m4 l4" f>p>r<"dt" t i>l<"clear">'
        });
        $('#mainstable_filter label').addClass('slabel');
        $('#bitstable').dataTable({
            responsive: true,
            "bLengthChange": false,
            "iDisplayLength": 20,
            "sDom": '<"row dt"<"col s12 m4 l4" f>p>r<"dt" t i>l<"clear">'
        });
        $('#bitstable_filter label').addClass('slabel');
    });
</script>
<script src="./js/login-regis.js" async></script>
</body>
</html>