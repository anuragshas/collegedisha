<?php
session_start();
if (isset($_SESSION['r']))
    unset($_SESSION['r']);
if (isset($_SESSION['c']))
    unset($_SESSION['c']);
if (isset($_SESSION['e']))
    unset($_SESSION['e']);
if (isset($_SESSION['s']))
    unset($_SESSION['s']);
require("navbar.php");
?>
<h4 class="center-align">For any Queries,Write To Us At</h4><br><h5 class="center-align">
    contact.collegedisha@gmail.com</h5><br><br>
<?php require("footer.php"); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/js/materialize.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script src="./js/additional-methods.min.js"></script>
<script>
    $(document).ready(function () {
        $(".button-collapse").sideNav();
        $('.modal-trigger').leanModal();
        $('.parallax').parallax();
    });
</script>
<script src="./js/login-regis.js" async></script>
</body>
</html>
