<?php
session_start();
if (!empty($_POST['submit'])) {
    if (substr($_POST['submit'], 0, 1) == 'q') {
        $qid = substr($_POST['submit'], 1);

        require_once "DBConnection.php";
        $dbconnection = new DBConnection();
        $connection = $dbconnection->connect();
        $link = $connection[0];
        $db = $connection[1];
        $qry = "DELETE FROM questions WHERE quesid=$qid";
        $results = mysqli_query($link, $qry);
        if ($results) {
            echo '<script>alert("Post Deleted Successfully")</script>';
            header("Refresh: 0.1; url=" . $_SESSION['cp']);
        } else {
            echo '<script>alert("Not Deleted")</script>';
            header("Refresh: 0.1; url=" . $_SESSION['cp']);
        }
        mysqli_close($link);
    } else if (substr($_POST['submit'], 0, 1) == 'a') {
        $aid = substr($_POST['submit'], 1);

        require_once "DBConnection.php";
        $dbconnection = new DBConnection();
        $connection = $dbconnection->connect();
        $link = $connection[0];
        $db = $connection[1];
        $qry = "DELETE FROM answers WHERE ansid=$aid";
        // echo $qry;
        $results = mysqli_query($link, $qry);
        if ($results) {
            echo '<script>alert("Post Deleted Successfully")</script>';
            header("Refresh: 0.1; url=" . $_SESSION['cp']);
        } else {
            echo '<script>alert("Not Deleted")</script>';
            header("Refresh: 0.1; url=" . $_SESSION['cp']);
        }
        mysqli_close($link);
    }
} else {
    echo "Data not entered";
}
?>