<?php
session_start();
$cat = $_SESSION['c'];
$rank = $_SESSION['r'];
if (!isset($_SESSION['auth'])) {
    $email = $_SESSION['e'];
}
if (isset($_POST['filter'])) {

    if ($_POST['group1'] == 'insname' && isset($_POST['iname']))
        $iname = $_POST['iname'];
    else
        $iname = null;
    if ($_POST['group1'] == 'brname' && isset($_POST['bname']))
        $bname = $_POST['bname'];
    else
        $bname = null;
    $chk = '';
    if ($iname && $_POST['group1'] == 'insname') {
        $chk .= ' institute_name IN (';
        for ($i = 0; $i < count($iname); $i++) {

            $chk .= '\'' . $iname[$i] . '\',';
            if ($i == count($iname) - 1)
                break;
        }
        $chk .= '\'' . $iname[$i] . '\')';

    } elseif ($bname && $_POST['group1'] == 'brname') {
        $chk .= ' branch_name IN (';
        for ($i = 0; $i < count($bname); $i++) {
            $chk .= '\'' . $bname[$i] . '\',';
            if ($i == count($bname) - 1)
                break;
        }
        $chk .= '\'' . $bname[$i] . '\')';
    }
}
switch ($cat) {
    case 1:
        $tname = "ja_general";
        $cname = "OpenRank";
        break;
    case 2:
        $tname = "ja_general";
        $cname = "PWDRank";
        break;
    case 3:
        $tname = "ja_obc";
        $cname = "OpenRank";
        break;
    case 4:
        $tname = "ja_obc";
        $cname = "PWDRank";
        break;
    case 5:
        $tname = "ja_sc";
        $cname = "OpenRank";
        break;
    case 6:
        $tname = "ja_sc";
        $cname = "PWDRank";
        break;
    case 7:
        $tname = "ja_st";
        $cname = "OpenRank";
        break;
    case 8:
        $tname = "ja_st";
        $cname = "PWDRank";
        break;
}
require_once "DBConnection.php";
$dbconnection = new DBConnection();
$connection = $dbconnection->connect();
$link = $connection[0];
$db = $connection[1];
if (isset($_POST['iname']) || isset($_POST['bname'])) {
    if (isset($_POST['filter'])) {//Create query
        $qry = 'SELECT institute_name,branch_name,' . $cname . ' FROM institute i inner join ' . $tname . ' b on i.id=b.id where ' . $cname . '>=' . $rank . ' and ' . $chk . ' order by ' . $cname;
    }

    $result = mysqli_query($link, $qry);
} else {

    $qry = $_SESSION['qry'];
    $result = mysqli_query($link, $qry);
}
if (isset($_POST['remove'])) {
    $qry = $_SESSION['qry'];
    $result = mysqli_query($link, $qry);
}
require("navbar.php");
?>
<div class="row">
    <h2 class="center-align" id="topmsg">JEE-ADVANCED COLLEGE PREDICTOR</h2>
    <div class="amber darken-2 headline "></div>
</div>
<br>

<div class="row">
    <div class="col s12 m12 l3">
        <div class="row">
            <div class="col s12 blue-grey darken-4 white-text">
                <h5 class="center-align">Try our Modified Search</h5>
            </div>
        </div>
        <div class="center-align">
            <?php
            if ($qry != $_SESSION['qry'])
                echo '<form class="col s12" action="advance_filter.php" method="post">
      <button class="btn-floating btn tooltipped waves-effect waves-light red" type="submit" data-position="bottom" data-delay="50" data-tooltip="Remove Filter" name="remove"><i class="material-icons">clear_all</i></button></form>';
            ?>
        </div>
        <div class="row">
            <form class="col s12" action="advance_filter.php" method="post">
                <div class="row">
                    <p>
                        <input name="group1" class="g1" type="radio" id="test1" value="insname" required/>
                        <label for="test1" id="insti" class="input-field col s12">

                            <select multiple name="iname[]" id="ins">
                                <option value="" disabled selected>I want this Institute</option>
                                <?php
                                $qry = 'SELECT distinct(institute_name) as institute_name FROM institute where id > 386 order by institute_name'; //Execute query
                                $result1 = mysqli_query($link, $qry);
                                while ($row = mysqli_fetch_assoc($result1)) {
                                    echo '<option value="' . $row['institute_name'] . '">' . $row['institute_name'] . '</option>';
                                }

                                ?>
                            </select>
                            <!-- <label for="ins">Institute Name</label> -->

                        </label>
                    </p>
                </div>
                <div class="row">
                    <p>
                        <input name="group1" class="g1" type="radio" id="test2" value="brname" required/>
                        <label for="test2" id="branch" class="input-field col s12">
                            <select multiple name="bname[]" id="bran">
                                <option value="" disabled selected>I want this Branch</option>
                                <?php
                                $qry = 'CALL getAdvancedBranch()'; //Execute query
                                $result1 = mysqli_query($link, $qry);
                                while ($row = mysqli_fetch_assoc($result1)) {
                                    echo '<option value="' . $row['branch_name'] . '">' . $row['branch_name'] . '</option>';
                                }

                                ?>
                            </select>
                            <!-- <label for="bran">Branch Name</label> -->

                        </label>
                    </p>
                </div>
                <br>
                <div class="row">
                    <div class="col s12">
                        <div class="center-align">
                            <button class="btn waves-effect waves-light " type="submit" name="filter"><i
                                        class="material-icons right">filter_list</i>Filter
                            </button>
                        </div>
                    </div>
                </div>
            </form>
            <div class="row">
                <div class="col s12">
                    <div class="center-align"><a id="res" class="btn waves-effect waves-light red lighten-2"><i
                                    class="material-icons right">restore</i>Reset</a></div>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <div class="center-align"><a href="index.php#predictor" class="btn waves-effect waves-light">Other
                            Predictors</a></div>
                </div>
            </div>
        </div>
    </div>


    <div class="col s12 m12 l8">


        <?php
        echo '<table class="bordered striped" id="advtable">
<thead><tr><th>Institute Name</th>
<th>Branch Name</th>
<th>Closing Ranks</th></tr></thead><tbody>';
        //Show the rows in the fetched resultset one by one
        while ($row = mysqli_fetch_assoc($result)) {
            echo '<tr>
<td>' . $row['institute_name'] . '</td>
<td>' . $row['branch_name'] . '</td>
<td>' . $row[$cname] . '</td>
</tr>';
        }
        echo '</tbody></table>';
        mysqli_close($link);
        ?>
    </div>
</div>

<?php require("footer.php"); ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/js/materialize.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.11/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script src="./js/additional-methods.min.js"></script>
<script>
    $(document).ready(function () {
        $(".button-collapse").sideNav();
        $('select').material_select();
        $('.modal-trigger').leanModal();
        $('.parallax').parallax();
        $('#advtable').dataTable({
            responsive: true,
            "bLengthChange": false,
            "iDisplayLength": 8,
            "aaSorting": [],
            "sDom": '<"row dt"<"col s12 m4 l4" f>p>r<"dt" t i>l<"clear">',
        });
        $('#advtable_filter label').addClass('slabel');
        $('.tooltipped').tooltip({delay: 50});
    });
    $('#res').click(function () {
        location.reload();
    });
</script>
<script src="./js/login-regis.js" async></script>
<script type="text/javascript">
    $("#test1").click(function () {
        $("#test2").prop("disabled", true);
        var op = document.getElementById("branch").getElementsByTagName("input");
        for (var i = 0; i < op.length; i++) {
            op[i].disabled = true;
        }
    });
    document.getElementById("test2").addEventListener("click", function () {
        document.getElementById("test1").setAttribute("disabled", "true");
        var op = document.getElementById("insti").getElementsByTagName("input");
        for (var i = 0; i < op.length; i++) {
            op[i].disabled = true;
        }
    });
</script>

</body>
</html>