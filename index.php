<?php
session_start();
if (isset($_SESSION['r']))
    unset($_SESSION['r']);
if (isset($_SESSION['c']))
    unset($_SESSION['c']);
if (isset($_SESSION['e']))
    unset($_SESSION['e']);
if (isset($_SESSION['s']))
    unset($_SESSION['s']);
require("navbar.php");
?>
<div id="mainpic">
    <div class="container">
        <div class="row">
            <div class="col s12 m12 l12" id="titlediv">
                <div class="tyd center-align"></div>
            </div>
        </div>
    </div>
</div>
<div class="pred " id="predictor">
    <h4 class="center-align white-text">COLLEGE PREDICTORS</h4>
</div>
<div class="container">
    <div class="row">
        <div class="col s12 m6 l4">
            <div class="card hoverable">
                <div class="card-image">
                    <img src="images/12.webp" width="25%" height="10%" alt="">
                </div>
                <div class="card-content">
                    <h5 class="center-align"><strong>JEE-MAIN</strong></h5>
                    <p>Joint Entrance Examination for admission into 30 NIT's, 18 IIIT's and other government funded
                        Engineering Institutes.
                        <br>Enter your JEE-Main Rank to know your chances of getting into these Colleges!!</p>
                </div>
                <div class="card-action">
                    <div class="bttn1 center-align">
                        <a class="btn-flat waves-effect waves-light modal-trigger white-text" data-target="modal1">Check
                            Here!</a>
                    </div>
                </div>
            </div>
        </div>
        <div id="modal1" class="modal">
            <div class="modal-content">
                <div class="row">
                    <form class="col s12 formValidate" id="mains" action="jeemains.php" method="post">
                        <div class="row">
                            <div class="input-field col s12">
                                <input name="rank" type="number" class="validate" min="1" required>
                                <label>Jee-Main/Predicted Rank</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12">
                                <label for="mainscat">Category *</label>
                                <select class="browser-default" id="mainscat" name="cat">
                                    <option value="" disabled selected>Select Your Category</option>
                                    <option value="1">General</option>
                                    <option value="2">General-PWD</option>
                                    <option value="3">OBC</option>
                                    <option value="4">OBC-PWD</option>
                                    <option value="5">SC</option>
                                    <option value="6">SC-PWD</option>
                                    <option value="7">ST</option>
                                    <option value="8">ST-PWD</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12">
                                <label for="state">State *</label>
                                <select class="browser-default" id="state" name="state">
                                    <option value="" disabled selected>Select Your Homestate</option>
                                    <?php
                                    require_once "DBConnection.php";
                                    $dbconnection = new DBConnection();
                                    $connection = $dbconnection->connect();
                                    $link = $connection[0];
                                    $db = $connection[1];
                                    $qry = 'CALL getState()';
                                    $result = mysqli_query($link, $qry);
                                    while ($row = mysqli_fetch_assoc($result))
                                        echo '<option value="' . $row['state'] . '">' . $row['state'] . '</option>';
                                    mysqli_close($link);
                                    ?>
                                </select>
                            </div>
                        </div>
                        <?php
                        if (!isset($_SESSION['auth'])) {
                            echo '<div class="row">
									<div class="input-field col s12">
										<input name="email" type="email" class="validate" >
										<label >Email</label>
									</div>
								</div>';
                        }
                        ?>
                        <button class="btn waves-effect modal-action waves-light" type="submit" name="submit">Predict
                            College
                        </button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col s12 m6 l4">
            <div class="card hoverable">
                <div class="card-image">
                    <img src="images/13.webp" width="25%" height="10%" alt="">
                </div>
                <div class="card-content">
                    <h5 class="center-align"><strong>JEE-ADVANCED</strong></h5>
                    <p>Joint Entrance Examination for admission into India's Best Engineering Institutions- IIT's(Indian
                        Institute of Technology)
                        <br>Just Enter Your JEE-Advance Rank to get your chances of getting into IIT's!!</p>
                </div>
                <div class="card-action">
                    <div class="bttn1 center-align">
                        <a class="btn-flat waves-effect waves-light modal-trigger white-text" data-target="modal2">Check
                            Here!</a>
                    </div>
                </div>
            </div>
        </div>
        <div id="modal2" class="modal">
            <div class="modal-content">
                <div class="row">
                    <form class="col s12 formValidate" name="advform" id="advform" action="advance.php" method="post">
                        <div class="row">
                            <div class="input-field col s12">
                                <input name="rank" type="number" class="validate" min="1" required>
                                <label>Enter Jee-Advance Category Rank</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12">
                                <label for="advcat">Category *</label>
                                <select name="cat" id="advcat" required class="browser-default">
                                    <option value="" disabled selected>Select Your Category</option>
                                    <option value="1">General</option>
                                    <option value="2">General-PWD</option>
                                    <option value="3">OBC</option>
                                    <option value="4">OBC-PWD</option>
                                    <option value="5">SC</option>
                                    <option value="6">SC-PWD</option>
                                    <option value="7">ST</option>
                                    <option value="8">ST-PWD</option>
                                </select>

                            </div>
                        </div>
                        <?php
                        if (!isset($_SESSION['auth'])) {
                            echo '<div class="row">
									<div class="input-field col s12">
										<input name="email" type="email" class="validate">
										<label >Email</label>
									</div>
								</div>';
                        }
                        ?>
                        <button class="btn waves-effect modal-action waves-light" type="submit" name="action">Predict
                            College
                        </button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col s12 m6 l4">
            <div class="card hoverable">
                <div class="card-image">
                    <img src="images/11.webp" width="25%" height="10%" alt="">
                </div>
                <div class="card-content">
                    <h5 class="center-align"><strong>BITSAT</strong></h5>
                    <p>Entrace Exam for admission into India's finest Private engineering college-BITS Pilani, Hyderabad
                        and Goa.
                        <br>Just Enter Your BITSAT score and get your chances of Getting into BITS!!</p>
                </div>
                <div class="card-action">
                    <div class="bttn1 center-align">
                        <a class="btn-flat waves-effect waves-light modal-trigger white-text" data-target="modal3">Check
                            Here!</a>
                    </div>
                </div>
            </div>
        </div>
        <div id="modal3" class="modal">
            <div class="modal-content">
                <div class="row">
                    <form class="col s12 formValidate" id="bitsform" name="bitsform" action="bits.php" method="post">
                        <div class="row">
                            <div class="input-field col s12">
                                <input name="rank" type="number" class="validate" required max="486" min="1">
                                <label>Enter Your Score</label>
                            </div>
                        </div>
                        <?php
                        if (!isset($_SESSION['auth'])) {
                            echo '<div class="row">
									<div class="input-field col s12">
										<input name="email" type="email" class="validate" >
										<label >Email</label>
									</div>
								</div>';
                        }
                        ?>
                        <button class="btn waves-effect modal-action waves-light" type="submit" name="action">Predict
                            College
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require("footer.php"); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/js/materialize.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/typed.js/1.1.1/typed.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script src="./js/additional-methods.min.js"></script>
<script>
    $(document).ready(function () {
        $('.modal-trigger').leanModal();
        $(".button-collapse").sideNav();
        $('.parallax').parallax();
    });
    $(function () {
        $(".tyd").typed({
            strings: ["<h1 id=\"cologo\">COLLEGE DISHA</h1> ^500 <h4 id=\"cologo2\">We Help You To Reach The Best!</h4>"],
            showCursor: false,
            typeSpeed: 50
        });
    });
</script>
<script src="./js/login-regis.js" async></script>
<script>
    $("#mains").validate({
        rules: {
            cat: "required",
            state: "required",
        },
        //For custom messages
        messages: {
            cat: "Please Select Your Category",
            state: "Please Select Your Home State",
            rank: "Please Enter Your Rank",
        },
        errorElement: 'div',
        errorPlacement: function (error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            }
            else {
                error.insertAfter(element);
            }
        }
    });
    $("#advform").validate({
        rules: {
            cat: "required",
        },
        //For custom messages
        messages: {
            cat: "Please Select Your Category",
            rank: "Please Enter Your Rank",
        },
        errorElement: 'div',
        errorPlacement: function (error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            }
            else {
                error.insertAfter(element);
            }
        }
    });
    $("#bitsform").validate({
        rules: {},
        //For custom messages
        messages: {
            rank: "Please Enter Your Marks",
        },
        errorElement: 'div',
        errorPlacement: function (error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            }
            else {
                error.insertAfter(element);
            }
        }
    });
</script>
</body>
</html>