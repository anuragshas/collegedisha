<?php
session_start();
if (isset($_SESSION['r']))
    unset($_SESSION['r']);
if (isset($_SESSION['c']))
    unset($_SESSION['c']);
if (isset($_SESSION['e']))
    unset($_SESSION['e']);
if (isset($_SESSION['s']))
    unset($_SESSION['s']);
require("navbar.php");
?>
<h2 class="center-align" id="topmsg">Privacy Policy</h2>
<div class="amber darken-2 headline "></div>
<br>
<br>
<div class="container">
    <p>Thanks a lot for visiting our web site. <br>
        Please read this privacy policy before using the site or submitting any personal information. By using the site,
        you are accepting the terms and conditions included in this privacy policy. Please review the privacy policy
        whenever you visit the site to make sure that you understand how any personal information you provide will be
        used.<br>
    <p>We collect information including name and email submitted by the users at the time of Registration.</p>

    <p>Collegedisha may use cookie and tracking technology depending on the features offered. Cookie and tracking
        technology are useful for gathering information such as browser type and operating system, tracking the number
        of visitors to the Site, and understanding how visitors use the Site. </p>

    <p>We may share information with governmental agencies or other companies assisting us in fraud prevention or
        investigation. We may do so when: (1) permitted or required by law; or, (2) trying to protect against or prevent
        actual or potential fraud or unauthorized transactions; The information is not provided to these companies for
        marketing purposes.</p>
    <p>Your personally identifiable information is kept secure. Only authorized employees, agents and contractors (who
        have agreed to keep information secure and confidential) have access to this information.</p>
    <p>For any further queries about our privacy policy you may contact us at:<br>
        contact.collegedisha@gmail.com<br>
        We reserve the right to make changes to this policy. Any changes to this policy will be posted.</p>
</div>
<br>
<?php require("footer.php"); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/js/materialize.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script src="./js/additional-methods.min.js"></script>
<script>
    $(document).ready(function () {
        $(".button-collapse").sideNav();
        $('.modal-trigger').leanModal();
        $('.parallax').parallax();
    });
</script>
<script src="./js/login-regis.js" async></script>
</body>
</html>
