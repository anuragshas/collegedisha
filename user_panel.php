<?php
session_start();
require("navbar.php");
?>
<div class="container">
    <?php
    if (!isset($_SESSION['auth'])) {
    } else if ($_SESSION['auth'] == 1) {
        echo '<div><h4 class="left-align" >Name- ' . $_SESSION['usrname'] . '</h4>';
        echo '<h4 class="left-align">Email -' . $_SESSION['usremail'] . '<h4></div><hr>';
        echo '<br><br><h4 class="left-align" >Questions Asked</h4>';
    }
    ?>
    <?php if (isset($_SESSION['auth']) && $_SESSION['auth'] == 1) {
        require_once "DBConnection.php";
        $dbconnection = new DBConnection();
        $connection = $dbconnection->connect();
        $link = $connection[0];
        $db = $connection[1];

        require_once "DBConnection.php";
        $dbconnection = new DBConnection();
        $connection = $dbconnection->connect();
        $link2 = $connection[0];
        $db2 = $connection[1];
        //Create query 
        $qry = 'SELECT * FROM questions where email=\'' . $_SESSION['usremail'] . '\' order by ques_time desc'; //Execute query
        $result = mysqli_query($link, $qry);
        while ($row = mysqli_fetch_assoc($result)) {


            echo '<div class="row hoverable z-depth-1 questions white">
                <div class="col s12 m12 l12">
                    <p class="blue-text">' . $row['email'] . '</p>
                    <p class="green-text"><i class="material-icons tiny">access_time</i>' . $row['ques_time'] . '</p><hr><br>
                    <p style="font-size:20px">' . $row['ques_stat'] . '</p><br>
                        <div class="right-align">
                            <a class="btn-flat waves-effect waves-light white-text modal-trigger" href="#ssq' . $row['quesid'] . '">Show Answers</a>
                        </div>
                </div>
            </div>';
            $qid = $row['quesid'];
            $qstat = $row['ques_stat'];

            $qry2 = 'SELECT * FROM answers where quesid= ' . $qid . ' order by ans_time desc ';
            $result2 = mysqli_query($link2, $qry2);
            echo '<div id="ssq' . $qid . '" class="modal bottom-sheet">
                        <div class="modal-content">
                            <h5>' . $qstat . '</h5>';

            //answer display
            if ($result2) {
                while ($row2 = mysqli_fetch_assoc($result2)) {
                    echo '<br><p class="blue-text">' . $row2['email'] . '</p>
                            <p class="green-text"><i class="material-icons tiny">access_time</i>' . $row2['ans_time'] . '</p><hr><br>
                            <div><p style="font-size:18px">' . $row2['ans_stat'] . '</p></div><br>';
                }
            }
            echo '<div class="modal-footer">';
            echo '<button class=" modal-trigger modal-close btn-flat waves-effect waves-light white-text" data-target="saq' . $row['quesid'] . '">Write Answer</button>';

            echo '</div>
                    </div>
                    </div>';
            //modal for answers
            echo '<div id="saq' . $row['quesid'] . '" class="modal">
                                        <div class="modal-content">
                                        <div class="row"><h5>Rules</h5></div>
                                        <div class="row">1. Please maintain the decorum of the forum. Do not ask or answer any question in abusive manner<br>
                                        2. Try to ask questions related to JEE MAINS, ADVANCED and BITSAT counselling only<br>
                                        3. Avoid any personal conversation on the forum<hr>
                                        </div>
                                            <div class="row">
                                              <form class="col s12 formValidate" action="post_qa.php" method="post">
                                                <div class="row">
                                                  <div class="input-field col s12">
                                                    <textarea id="textarea1" name="answ" class="materialize-textarea" length="1000" maxlength="1000" required></textarea>
                                                    <label for="textarea1">Write Your Answer Here.</label>
                                                  </div>
                                                </div>
                                                <button class="btn-flat waves-effect waves-light white-text" type="submit" name="submit" value="' . $row['quesid'] . '"><i class="material-icons right">send</i>Submit</button>
                                              </form>
                                            </div>
                                        </div> 
                                    </div>';

        }
        mysqli_close($link);
        mysqli_close($link2);

        echo '<br><br><h4 class="left-align" >Questions Answered / Commented</h4>';
        require_once "DBConnection.php";
        $dbconnection = new DBConnection();
        $connection = $dbconnection->connect();
        $link = $connection[0];
        $db = $connection[1];

        require_once "DBConnection.php";
        $dbconnection = new DBConnection();
        $connection = $dbconnection->connect();
        $link2 = $connection[0];
        $db2 = $connection[1];

        //Create query 
        $qry = 'SELECT distinct(q.quesid) as aquid, q.email,ques_time,ques_stat FROM questions q inner join answers a on q.quesid=a.quesid where a.email=\'' . $_SESSION['usremail'] . '\' order by ques_time desc'; //Execute query
        $result = mysqli_query($link, $qry);
        while ($row = mysqli_fetch_assoc($result)) {


            echo '<div class="row hoverable z-depth-1 questions white">
                <div class="col s12 m12 l12">
                    <p class="blue-text">' . $row['email'] . '</p>
                    <p class="green-text"><i class="material-icons tiny">access_time</i>' . $row['ques_time'] . '</p><hr><br>
                    <p style="font-size:20px">' . $row['ques_stat'] . '</p><br>
                        <div class="right-align">
                            <a class="btn-flat waves-effect waves-light white-text modal-trigger" href="#qa' . $row['aquid'] . '">Show Answers</a>
                        </div>
                </div>
            </div>';
            $qid = $row['aquid'];
            $qstat = $row['ques_stat'];

            $qry2 = 'SELECT * FROM answers where quesid= ' . $qid . ' order by ans_time desc ';
            $result2 = mysqli_query($link2, $qry2);
            echo '<div id="qa' . $qid . '" class="modal bottom-sheet">
                        <div class="modal-content">
                            <h5>' . $qstat . '</h5>';

            //answer display
            if ($result2) {
                while ($row2 = mysqli_fetch_assoc($result2)) {
                    echo '<br><p class="blue-text">' . $row2['email'] . '</p>
                            <p class="green-text"><i class="material-icons tiny">access_time</i>' . $row2['ans_time'] . '</p><hr><br>
                            <div><p style="font-size:18px">' . $row2['ans_stat'] . '</p></div><br>';
                }

            }
            echo '<div class="modal-footer">';
            echo '<button class=" modal-trigger modal-close btn-flat waves-effect waves-light white-text" data-target="aqa' . $row['aquid'] . '">Write Answer</button>';

            echo '</div>
                    </div>
                    </div>';
            //modal for answers
            echo '<div id="aqa' . $row['aquid'] . '" class="modal">
                                        <div class="modal-content">
                                        <div class="row"><h5>Rules</h5></div>
                                        <div class="row">1. Please maintain the decorum of the forum. Do not ask or answer any question in abusive manner<br>
                                        2. Try to ask questions related to JEE MAINS, ADVANCED and BITSAT counselling only<br>
                                        3. Avoid any personal conversation on the forum<hr>
                                        </div>
                                            <div class="row">
                                              <form class="col s12 formValidate" action="post_qa.php" method="post">
                                                <div class="row">
                                                  <div class="input-field col s12">
                                                    <textarea id="textarea1" name="answ" class="materialize-textarea" length="1000" maxlength="1000" required></textarea>
                                                    <label for="textarea1">Write Your Answer Here.</label>
                                                  </div>
                                                </div>
                                                <button class="btn-flat waves-effect waves-light white-text" type="submit" name="submit" value="' . $row['aquid'] . '"><i class="material-icons right">send</i>Submit</button>
                                              </form>
                                            </div>
                                        </div> 
                                    </div>';

        }
        mysqli_close($link);
        mysqli_close($link2);
    }
    ?>

</div>
<?php require("footer.php"); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/js/materialize.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script src="./js/additional-methods.min.js"></script>
<script>
    $(document).ready(function () {
        $('.modal-trigger').leanModal();
        // $('select').material_select();
        $(".button-collapse").sideNav();

    });</script>

</body>
</html>