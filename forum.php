<?php
session_start();
require("navbar.php");
?>
<div class="row">
    <h2 class="center-align" id="topmsg">Questions &amp; Answers Forum</h2>
    <div class="amber darken-2 headline "></div>
</div>
<br>
<div class="container">
    <div class="row">
        <div class="center-align">
            <?php
            if (!isset($_SESSION['auth'])) echo '<a class="btn waves-effect waves-light large modal-trigger" data-target="log"><i class="material-icons left">help</i>ASK A QUESTION</a>';
            else if ($_SESSION['auth'] == 1) echo '<a class="btn waves-effect waves-light large modal-trigger" data-target="ask"><i class="material-icons left">help</i>ASK A QUESTION</a>';
            ?>
        </div>
    </div>
    <?php
    require_once "DBConnection.php";
    $dbconnection = new DBConnection();
    $connection = $dbconnection->connect();
    $link = $connection[0];
    $db = $connection[1];
    require_once "DBConnection.php";
    $dbconnection = new DBConnection();
    $connection = $dbconnection->connect();
    $link2 = $connection[0];
    $db2 = $connection[1];
    $qry = 'SELECT * FROM questions order by ques_time desc'; //Execute query
    $result = mysqli_query($link, $qry);
    while ($row = mysqli_fetch_assoc($result)) {
        if (!isset($_SESSION['auth']) || $_SESSION['auth'] == 1) {
            echo '<div class="row hoverable z-depth-1 questions white">
						<div class="col s12 m12 l12">
							<p class="blue-text">' . $row['email'] . '</p>
							<p class="green-text"><i class="material-icons tiny">access_time</i>' . $row['ques_time'] . '</p><hr><br>
							<p style="font-size:20px">' . $row['ques_stat'] . '</p><br>
							<div class="right-align">
							<a class="btn-flat waves-effect waves-light white-text modal-trigger" href="#q' . $row['quesid'] . '">Show Answers</a>
							</div>
						</div>
					</div>';
        } else if ($_SESSION['auth'] == 2) {
            echo '<div class="row hoverable z-depth-1 questions white">
						<div class="col s12 m12 l12">
							<p class="blue-text">' . $row['email'] . '</p>
							<p class="green-text"><i class="material-icons tiny">access_time</i>' . $row['ques_time'] . '</p><hr><br>
							<p style="font-size:20px">' . $row['ques_stat'] . '</p><br>
								<form action="del_post.php" method="post"><button class="red waves-light btn-flat white-text" type="submit" name="submit" value="q' . $row['quesid'] . '"><i class="material-icons">delete_forever</i></button></form>
								<div class="right-align">
									<a class="btn-flat waves-effect waves-light white-text modal-trigger" href="#q' . $row['quesid'] . '">Show Answers</a>
								</div>
						</div>
					</div>';
        }
        $qid = $row['quesid'];
        $qstat = $row['ques_stat'];
        $qry2 = 'SELECT * FROM answers where quesid=' . $qid . ' order by ans_time desc ';
        $result2 = mysqli_query($link2, $qry2);
        echo '<div id="q' . $qid . '" class="modal bottom-sheet">
							<div class="modal-content">
								<h5>' . $qstat . '</h5>';
        //answer display
        if ($result2) {
            while ($row2 = mysqli_fetch_assoc($result2)) {
                if (!isset($_SESSION['auth']) || $_SESSION['auth'] == 1) {
                    echo '<br><p class="blue-text">' . $row2['email'] . '</p>
								<p class="green-text"><i class="material-icons tiny">access_time</i>' . $row2['ans_time'] . '</p><hr><br>
								<div><p style="font-size:18px">' . $row2['ans_stat'] . '</p></div><br>';
                } else if ($_SESSION['auth'] == 2) {
                    echo '<br><p class="blue-text">' . $row2['email'] . '</p>
								<p class="green-text"><i class="material-icons tiny">access_time</i>' . $row2['ans_time'] . '</p><hr><br>
								<div><p style="font-size:18px">' . $row2['ans_stat'] . '</p></div><br>';
                    echo '<form action="del_post.php" method="post"><button class="red waves-light btn-flat white-text" type="submit" name="submit" value="a' . $row2['ansid'] . '"><i class="material-icons">delete</i></button></form><br>';
                }
            }
        }
        echo '<div class="modal-footer">';
        if (!isset($_SESSION['auth'])) {
            echo '<button class=" modal-trigger modal-close btn-flat waves-effect waves-light white-text" data-target="log">Write Answer</button>';
        } else if ($_SESSION['auth'] == 1) {
            echo '<button class=" modal-trigger modal-close btn-flat waves-effect waves-light white-text" data-target="aq' . $row['quesid'] . '">Write Answer</button>';
        }
        echo '</div>
					</div>
				</div>';
        //modal for answers
        echo '<div id="aq' . $row['quesid'] . '" class="modal">
							<div class="modal-content">
								<div class="row"><h5>Rules</h5></div>
									<div class="row">1. Please maintain the decorum of the forum. Do not ask or answer any question in abusive manner<br>
									2. Try to ask questions related to JEE MAINS, ADVANCED and BITSAT counselling only<br>
									3. Avoid any personal conversation on the forum<hr>
								</div>
								<div class="row">
									<form class="col s12 formValidate" action="post_qa.php" method="post">
										<div class="row">
										  <div class="input-field col s12">
											<textarea id="textarea1" name="answ" class="materialize-textarea" length="1000" maxlength="1000" required></textarea>
												<label for="textarea1">Write Your Answer Here.</label>
											</div>
										</div>
										<button class="btn-flat waves-effect waves-light white-text" type="submit" name="submit" value="' . $row['quesid'] . '"><i class="material-icons right">send</i>Submit</button>
									</form>
								</div>
							</div> 
						</div>';
    }
    mysqli_close($link);
    mysqli_close($link2);
    ?>
</div>
<div id="ask" class="modal">
    <div class="modal-content">
        <div class="modal-content">
            <div class="row"><h5>Rules</h5></div>
            <div class="row">1. Please maintain the decorum of the forum. Do not ask or answer any question in abusive
                manner<br>
                2. Try to ask questions related to JEE MAINS, ADVANCED and BITSAT counselling only<br>
                3. Avoid any personal conversation on the forum
                <hr>
            </div>
            <div class="row">
                <form class="col s12 formValidate" action="post_qa.php" method="post">
                    <div class="row">
                        <div class="input-field col s12">
                            <textarea id="textarea2" name="ques" class="materialize-textarea" length="500"
                                      maxlength="500" required></textarea>
                            <label for="textarea2">Ask Your Question Here.</label>
                        </div>
                    </div>
                    <button class="btn-flat waves-effect waves-light white-text" type="submit" name="submit"
                            value="quessub"><i class="material-icons right">send</i>Submit
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
<br>
<?php require("footer.php"); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/js/materialize.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script src="./js/additional-methods.min.js"></script>
<script>
    $(document).ready(function () {
        $(".button-collapse").sideNav();
        $('.modal-trigger').leanModal();
        <?php
        if (isset($_SESSION['quesopen'])) {
            echo '$(\'#q' . $_SESSION['quesopen'] . '\').openModal();';
            unset($_SESSION['quesopen']);
        }
        ?>
    });
</script>
<script src="./js/login-regis.js" async></script>
</body>
</html>