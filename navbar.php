<?php
$url_name=$_SERVER["REQUEST_URI"];
$_SESSION['cp']=$url_name;
?>
<!doctype html>
	<head>
		<meta name="charset" content="UTF-8">
		<?php
			if($url_name=='/forum.php')
				echo'<meta name="description" content="College Disha - Question and Answers Forum for JEE Mains, JEE Advanced, BITSAT">
		<meta name="keywords" content="college predictor, college disha, college rank predictor, college, jee mains predictor, jee main predictor, jee advanced predictor, forum">';
			else
				echo'<meta name="description" content="College Disha - College Predictor for JEE Mains, JEE Advanced and BITSAT">
		<meta name="keywords" content="college predictor, college disha, college rank predictor, college, jee mains predictor, jee main predictor, jee advanced predictor">';
		?>
		<meta name="robots" content="index, follow">
		<meta name="googlebot" content="index, follow">
		<meta name="robots" content="NOODP">
		<meta name="robots" content="NOYDIR">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="theme-color" content="#001720">
		<?php
			if($url_name=='/bits.php' || $url_name=='/bits_filter.php')
				echo'<title>College Predictor for BITSAT | CollegeDisha</title>';
			else if($url_name=='/jeemains.php' || $url_name=='/jeemain_filter.php')
				echo'<title>College Predictor for JEE Mains | CollegeDisha</title>';
			else if($url_name=='/advance.php' || $url_name=='/advance_filter.php')
				echo'<title>College Predictor for JEE Advanced | CollegeDisha</title>';
			else if($url_name=='/forum.php')
				echo'<title>Question and Answers Forum | CollegeDisha</title>';
			else if($url_name=='/user_panel.php')
				echo'<title>'.$_SESSION["usrname"].' | CollegeDisha</title>';
			else if($url_name=='/contact-us.php')
				echo'<title>Contact Us | CollegeDisha</title>';
			else if($url_name=='/privacy-policy.php')
				echo'<title>Privacy Policy | CollegeDisha</title>';
			else
				echo'<title>College Predictor for JEE Mains, JEE Advanced and BITSAT | CollegeDisha</title>';
		?>
		<link rel="shortcut icon" href="./images/logo.webp"/>
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
		<link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet'/>
		<?php
			if($url_name=='/index.php' || $url_name=='/')
				echo'<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"/>';?>
		<link href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/css/materialize.min.css" rel="stylesheet"/>
		<?php if(!($url_name=='/index.php' || $url_name=='/' ||
		$url_name=='/forum.php' || $url_name=='/user_panel.php' ||
		$url_name=='/privacy-policy.php' || $url_name=='/contact-us.php'))
			echo'<link href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.11/css/jquery.dataTables.min.css" rel="stylesheet"/>';
		?>
		<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet"/>
		<link href="./css/style.css" rel="stylesheet"/>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js"></script>
		<script type="text/javascript">
		$(window).load(function() {
		$("#spinner").fadeOut("slow");
		})
		</script>
	</head>
	<body>
		<!-- Google Tag Manager -->
		<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-NG4DHZ"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-NG4DHZ');</script>
		<!-- End Google Tag Manager -->
		<div id="spinner"></div>
		<div class="navbar-fixed">
			<nav id="mynav">
				<div class="nav-wrapper">
					<a href="./" class="brand-logo"><img class="hide-on-med-and-down" src="./images/navlogo.webp" width="102" height="50" alt=""><span id="brandname">COLLEGE DISHA</span></a>
					<a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
					<ul class="right hide-on-med-and-down">
						<?php
						if($url_name=='/impinfo.php')
							echo'<li><a href="index.php#predictor">Predictors</a></li>';
						else
							echo'<li><a href="impinfo.php">Important Info</a></li>';
						echo'<li><a href="forum.php">Forum</a></li>';
						if(!isset($_SESSION['auth']))
							echo '<li><a class="modal-trigger" data-target="log">Login</a></li>
						<li><a class="modal-trigger" data-target="reg">Register</a></li>';
						else if($_SESSION['auth']==1)
							echo '<li><a href="user_panel.php">'.substr($_SESSION["usrname"], 0, 15).'</a></li>
							<li><a href="logout.php">Log Out</a></li>';
						else if($_SESSION['auth']==2)
							echo '<li><a href="#">'.substr($_SESSION["usrname"], 0, 15).'</a></li>
							<li><a href="logout.php">Log Out</a></li>';
						?>
					</ul>
					 <ul class="side-nav" id="mobile-demo">
						<li><a href="./">Home</a></li>
						<?php
							if($url_name=='/impinfo.php')
								echo'<li><a href="index.php#predictor">Predictors</a></li>';
							else
								echo'<li><a href="impinfo.php">Important Info</a></li>';
							echo'<li><a href="forum.php">Forum</a></li>';
							if(!isset($_SESSION['auth']))
								echo '<li><a class="modal-trigger" data-target="log">Login</a></li>
								<li><a class="modal-trigger" data-target="reg">Register</a></li>';
							else if($_SESSION['auth']==1)
								echo '<li><a href="user_panel.php">'.substr($_SESSION["usrname"], 0, 15).'</a></li>
								<li ><a href="logout.php">Log Out</a></li>';
							else if($_SESSION['auth']==2)
								echo '<li><a href="#">'.substr($_SESSION["usrname"], 0, 15).'</a></li>
								<li ><a href="logout.php">Log Out</a></li>';
						?>
					</ul>
				</div>
			</nav>
		</div>
		<div id="reg" class="modal">
			<div class="modal-content">
				<div class="row">
					<div class="col s12">
						<ul class="tabs">
							<li class="tab col s12"><a class ="active" href="#regist">Register</a></li>
						</ul>
					</div>
					<div id="regist" class="col s12">
						<div class="row">
							<form class="formValidate" id="regis" method="post" action="register.php" novalidate="novalidate">
								<div class="row">
									<div class="input-field col s12">
										<i class="material-icons prefix">account_circle</i>
										<label for="uname" class="">Name*</label>
										<input id="uname" name="uname" type="text" pattern="[a-zA-Z][a-zA-Z ]+" data-error=".errorTxt4">
										<div class="errorTxt4"></div>
									</div>
									<div class="input-field col s12">
										<i class="material-icons prefix">email</i>
										<label for="cemail" class="">E-Mail *</label>
										<input id="cemail" type="email" name="cemail" data-error=".errorTxt1">
										<div class="errorTxt1"></div>
									</div>
									<div class="input-field col s12">
										<i class="material-icons prefix">lock_outline</i>
										<label for="pwd" class="">Password *</label>
										<input id="pwd" type="password" name="pwd" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" data-error=".errorTxt2">
										<div class="errorTxt2"></div>
									</div>
									<div class="input-field col s12">
										<i class="material-icons prefix">lock</i>
										<label for="cpassword" class="">Confirm Password *</label>
										<input id="cpassword" type="password" name="cpassword" data-error=".errorTxt3">
										<div class="errorTxt3"></div>
									</div>
									<div class="input-field col s12">
										<button class="btn waves-effect waves-light left" type="submit" name="registerbtn">Submit<i class="material-icons right">send</i>
										</button>
									</div>
									<br>
								</div>
							</form>
							<div>Already Registered, <a class="modal-trigger modal-close" data-target="log">Click Here</a> To Login</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="log" class="modal">
			<div class="modal-content">
				<div class="row">
					<div class="col s12">
						<ul class="tabs">
							<li class="tab col s6"><a href="#testusr">Login</a></li>
						</ul>
					</div>
					<div id="testusr" class="col s12">
						<div class="row">
							<form class="formValidate" id="loginusr" method="post" action="login.php" novalidate="novalidate">
								<div class="row">
									<div class="input-field col s12">
										<i class="material-icons prefix">email</i>
									  <label for="useremail" class="">E-Mail *</label>
									  <input id="useremail" type="email" name="useremail" data-error=".errorTxt5">
									  <div class="errorTxt5"></div>
									</div>
									<div class="input-field col s12">
										<i class="material-icons prefix">lock_open</i>
									  <label for="usrpassword" class="">Password *</label>
									  <input id="usrpassword" type="password" name="usrpassword" data-error=".errorTxt6">
									  <div class="errorTxt6"></div>
									</div>
									<div class="input-field col s12">
										<button class="btn waves-effect waves-light left" type="submit" name="usrloginbtn">Login<i class="material-icons right">send</i>
										</button>
									</div>
								</div>
							</form>
							<div>Not Registered, <a class="modal-trigger modal-close" data-target="reg">Click Here</a> To Register.</div>
						</div>
					</div>
				</div>
			</div>
		</div>